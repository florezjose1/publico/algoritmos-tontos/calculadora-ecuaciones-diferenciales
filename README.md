#### Calculadora de ecuaciones diferenciales

A continuación, verás solo un pequeño demo de una calculadora para resolver ecuaciones diferenciales.


Demo:

![Welcome](media/welcome.png)


![Calculator](media/calculator.png)



Hecho con ♥ por [Jose Florez, Frank Moncada](https://joseflorez.co)
